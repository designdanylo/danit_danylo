let activeServices = document.querySelector('.os-nav .active');
const nameList = document.querySelector('.os-nav');

let selectedWorkTipe = document.querySelector('.work-nav .active');
const workListConteiner = document.querySelector('.work-nav');

let currentWorckCategory = document.querySelectorAll('.work-img-conteiner .work-img');

let visibleImg = 12;

let avatarOrder = 0;

const avatarListConteiner = document.querySelector('.avatar-conteiner');

let activeAvatar = document.querySelector('.avatar-conteiner .active')

const people = [{
  login: 'hasanAli',
  name: 'Hasan Ali',
  position: 'UX Designer',
  review: 'Оdio vehicula eget. Vivamus congue ante ac eleifend porta. Fusce vitae nulla id neque elementum eleifend vitae ac leo.',
},
{
  login: 'doriMur',
  name: 'Dori Mur',
  position: 'Graphic Design',
  review: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam metus dolor, sodales non feugiat a, tincidunt nec odio. Cras porta elit a ultricies mollis. Morbi suscipit laoreet mi, in',
},
{
  login: 'loyBega',
  name: 'Loy Bega',
  position: 'Musical',
  review: 'Morbi suscipit laoreet mi, in laoreet odio vehicula eget. Vivamus congue ante ac eleifend porta. Fusce vitae nulla id neque elementum eleifend vitae ac leo.',
},
{
  login: 'anitaYa',
  name: 'Anita Ya',
  position: 'Programer',
  review: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam metus dolor, sodales non feugiat a, tincidunt nec odio. Cras porta elit a ultricies mollis. Morbi suscipit laoreet mi, in laoreet odio vehicula eget. Vivamus congue ante ac eleifend porta. Fusce vitae nulla id neque elementum eleifend vitae ac leo.'
}
];


document.addEventListener('click', (event)=>{
  if (event.target.classList.contains("os-nav-items")) switchServices(event.target);
  if (event.target.classList.contains("work-nav-items")) switchWork(event.target);
  if (event.target.closest('.amazing-bg-color .main-btn')) loadMoreImg(event.target, currentWorckCategory, document.querySelectorAll('.amazing-bg-color .show'));

  if (event.target.closest('.avatar-small')) {
    switchAvatar(event.target.closest('.avatar-small'));
    switchBigAvatar(event.target.closest('.avatar-small'));
    switchReview(event.target.dataset.userName);
  };
  if (event.target.classList.contains("btn-arrow")) {
    avatarOrder = activeAvatar.firstElementChild.dataset.order;
    checkSliderBtn(event.target.id)
  }

  
});

function switchServices(tab) {
  if (activeServices)  activeServices.classList.remove('active');

  activeServices = tab;
  activeServices.classList.add('active')
  
  document.querySelectorAll(`.os-nav-descr`).forEach(element => {
    element.dataset.switchtext === activeServices.dataset.switch ? element.classList.add('active') : element.classList.remove('active');
  });
}

function switchWork(tab) {
  selectedWorkTipe.classList.remove('active');
  selectedWorkTipe = tab;
  selectedWorkTipe.classList.add('active')
  const loadBtn = document.querySelector('.amazing-bg-color .main-btn');

  document.querySelectorAll(`.work-img-conteiner .work-img img `).forEach(element => {
    if (selectedWorkTipe.dataset.switch === 'all') {
      element.classList.remove('hide');
      currentWorckCategory = document.querySelectorAll('.work-img-conteiner .work-img');
    } else if (element.getAttribute('src').includes(`${selectedWorkTipe.dataset.switch}`)) {
      element.classList.remove('hide');
      currentWorckCategory = document.querySelectorAll(`.work-img-conteiner [src*=${selectedWorkTipe.dataset.switch}]`);
    } else {
      element.classList.add('hide');
    }
  })
  trimm(currentWorckCategory)
  currentWorckCategory.length <= 12 ? loadBtn.classList.add('hide') : loadBtn.classList.remove('hide')
}

function trimmWorkImg(value) {
  [].forEach.call(value, function (item, index) {
    if (index > 12 - 1) {
      item.classList.remove('show')
      item.classList.add('hide')
    }
  })
}

function loadMoreImg(btn, array, quantity) {
  const imgList = [...array];

  for (let i = quantity.length; i < quantity.length + 12; i++) {
    if (imgList[i]) imgList[i].classList.replace('hide', 'show');
  }
  if (quantity.length + 12 >= imgList.length && btn.classList.contains("main-btn")) btn.classList.add('hide');
}

function switchAvatar(tab) {
  activeAvatar.classList.remove('active')
  activeAvatar = tab;
  activeAvatar.classList.add('active')
}

function switchBigAvatar(avatarlink) {
  const avatarConteiner = document.querySelector('.avatar-circle');
  avatarConteiner.classList.add('animate');

  setTimeout(function () {
    avatarConteiner.innerHTML = avatarlink.innerHTML;
    avatarConteiner.classList.remove('animate')
  }, 400)
}

function switchReview(login) {
  people.forEach(element => {
    if (element.login === login) {
      document.getElementById('userReview').innerHTML = `${element.review}`;
      document.getElementById('userName').innerHTML = `${element.name}`;
      document.getElementById('userDirection').innerHTML = `${element.position}`;
    }
  });
}

function checkSliderBtn(btn) {
  activeAvatar.classList.remove('active');
  if (btn === 'nextBtn') {
    avatarOrder < 3 ? avatarOrder++ : avatarOrder = 0;
  }
  if (btn === 'previousBtn') {
    avatarOrder > 0 ? avatarOrder-- : avatarOrder = 3;
  }
  switchAvatarByBtn(avatarOrder)
}

function switchAvatarByBtn(value) {
  activeAvatar = document.querySelector(`[data-order=\'${value}\']`).closest('.avatar-small')
  activeAvatar.classList.add('active');
  switchReview(activeAvatar.firstElementChild.dataset.userName)
  switchBigAvatar(activeAvatar.closest('.avatar-small'));
}

trimmWorkImg(currentWorckCategory);
switchReview(activeAvatar.firstElementChild.dataset.userName);
switchBigAvatar(activeAvatar.closest('.avatar-small'));

