const getIpUrl = "https://api.ipify.org/?format=json";
const button = document.querySelector(".button");
const container = document.querySelector(".container");

button.addEventListener("click", () => {
  getIp();
});
function getIp() {
  (async () => {
    try {
      const response = await fetch(getIpUrl);
      const data = await response.json();
      checkIp(data.ip);
    } catch (error) {
      alert(error);
    }
  })();
}
function checkIp(value) {
  let checkIPUrl = `http://ip-api.com/json/${value}`;
  (async () => {
    try {
      const response = await fetch(checkIPUrl);
      const data = await response.json();
      renderLocation(data);
    } catch (error) {
      alert(error);
    }
  })();
}
function renderLocation(object) {
    container.innerHTML = `
    <h1>You're busted </h1>
    <p><span>Continent:</span> ${object.timezone.split("/")[0]}</p>
    <p><span>Country:</span> ${object.country}</p>
    <p><span>Region:</span> ${object.regionName}</p>
    <p><span>City:</span> ${object.city}</p>
    <p><span>District:</span> ${object.zip}</p>
    `;
}
