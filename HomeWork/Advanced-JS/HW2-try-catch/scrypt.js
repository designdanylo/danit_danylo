
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const verifiableProperties = ["name", "author", "price"];
const container = document.querySelector("#root");

function checkProperties(obj) {
  for (const property of verifiableProperties) {
    if (!(property in obj)) {
      throw new Error(
        `В обьекте ${JSON.stringify(obj)} нету cвойства '${property}'`
      );
    }
  }
  return true;
}

function getBooks() {
  return books.filter((element) => {
    try {
      return checkProperties(element);
    } catch (error) {
      console.log(error.message);
    }
  });
}

function createBookList(parent, array) {
  const ul = document.createElement("ul");
  parent.appendChild(ul);

  array.forEach((book) => {
    ul.insertAdjacentHTML(
      "beforeend",
      `<li style="margin-bottom: 15px">${book.author}: "${book.name}" <div>Цена: ${book.price} грн.</div></li>`
    );
  });
}

createBookList(container, getBooks());
