class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(newName) {
    this._name = newName;
  }
  get age() {
    return this._age;
  }
  set age(newAge) {
    this._age = newAge;
  }

  get salary() {
    return this._salary;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor({ name, age, salary, lang }) {
    super(name, age, salary);
    this.lang = lang;
  }
  set salary(newValue) {
    this._salary = newValue;
  }
  get salary() {
    return this._salary * 3;
  }
  employeeDetails() {
    return [this.name, this.age, this.salary, this.lang];
  }
}


const programmer1 = new Programmer({
  name: "Tina",
  age: 20,
  salary: 43000,
  lang: ["РНР", "Go", "JavaScript"],
});
const programmer2 = new Programmer({
  name: "Danylo",
  age: 21,
  salary: 55000,
  lang: {
    ru: true,
    ua: false,
    eng: true,
    de: false,
    programming: ["React", "HTML", "Java", "CSS", "Python"],
  },
});
const programmer3 = new Programmer({
  name: "Ivan",
  age: 37,
  salary: 2300000,
  lang: {
    ru: true,
    ua: false,
    eng: true,
    de: false,
    programming: ["C#", "Fortra"],
  },
});
const programmer4 = new Programmer({
  name: "Arthem",
  age: 24,
  salary: 7000000,
  lang: {
    ru: false,
    ua: true,
    eng: true,
    de: false,
    programming: ["РНР", "Go", "JavaScript"],
  },
});

console.log(programmer1.employeeDetails());
console.log(programmer2.employeeDetails());
console.log(programmer3.employeeDetails());
console.log(programmer4.employeeDetails());
