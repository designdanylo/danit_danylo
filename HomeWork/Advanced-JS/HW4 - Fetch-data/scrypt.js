
const list = document.createElement("ul");
document.body.append(list);

const requestURL = "https://ajax.test-danit.com/api/swapi/films";


fetch(requestURL)
  .then((response) => response.json())
  .then((data) => renderList(data));

function createList(array, element) {
  element.innerHTML = `<p style="font-size: 20px; font-weight: bold; color: purple;">Episode: ${array.episodeId}</p>
  <p style="font-weight: bold">Title: <span style="font-weight: normal"> ${array.name}</span></p>
  <p style="font-weight: bold;" >Opening crawl:  <span style="font-weight: normal;"> ${array.openingCrawl}</span></p>`;
}

function createCharsctersList(text, parrent) {
  const list = document.createElement("li");
  list.style.listStyle="none"
  list.innerHTML = `<p style="font-weight: bold;";>Characters: <span style="font-style: italic; font-weight: normal;">${text}</span></p>`;
  parrent.appendChild(list);
}
function renderList(array) {
  array.forEach((item) => {
    const film = document.createElement("li");
    let characterList = "";

    (async () => {
      for (let i = 0; i < item.characters.length; i++) {
        const response = await fetch(item.characters[i]);
        const data = await response.json();
        const name = await data.name;

        characterList += `${name}`;
        if (i !== item.characters.length - 1) {
          characterList += `, `;
        }
      }

      createCharsctersList(characterList, film); 
    })();

    createList(item, film);
    list.appendChild(film);
  });
}
