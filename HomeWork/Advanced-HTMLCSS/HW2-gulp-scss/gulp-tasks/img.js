// const { src } = require("gulp");
const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const newer = require("gulp-newer");

exports.img = () => {
  return gulp.src("src/img/**/*")
  .pipe(newer("dist/img/**"))
  .pipe(imagemin({
    verbose: true
  }))
  .pipe(gulp.dest('dist/img'))
};