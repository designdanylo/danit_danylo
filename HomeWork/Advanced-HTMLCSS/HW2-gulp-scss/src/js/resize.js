["resize", "load"].forEach(function (event) {
  window.addEventListener(event, function () {
          callBackOnMediaChange();
    },false);
});

const showPosts = () =>
  document.querySelector(".post-wrapper:last-child").classList.remove("hide");
const hidePosts = () =>
  document.querySelector(".post-wrapper:last-child").classList.add("hide");

function callBackOnMediaChange() {
  if (window.innerWidth >= 780) {
    showDescription();
    showPosts();
    showInstlink();
    window.innerWidth > 1200 ? toggleQuantityImg(4) : toggleQuantityImg(3);
  } else {
    hideDescription();
    hidePosts();
    hideInstlink();
    toggleQuantityImg(1);
  }
}



function hideDescription() {
  document
    .querySelectorAll(".post-wrapper .description-text")
    .forEach((element) => {
      element.classList.add("hide");
    });
}

function showDescription() {
  document
    .querySelectorAll(".post-wrapper .description-text")
    .forEach((element) => {
      element.classList.remove("hide");
    });
}

function toggleQuantityImg(num) {
  const shotsContein = document.querySelector(".main-shots-img-conteiner");
  let lastShots = document.querySelector(
    ".main-shots-img-conteiner img:last-child"
  );
  let quantityImg = document.querySelectorAll(
    ".main-shots-img-conteiner img"
  ).length;

  while (quantityImg > num) {
    shotsContein.removeChild(lastShots);
  }
  while (quantityImg < num) {
    quantityImg++;
    shotsContein.insertAdjacentHTML("beforeend", createNewImgElement(quantityImg));
  }
}

function createNewImgElement(num) {
  return `<img src="./img/shots000${num}.png" alt="instagram shot">`;
}

function showInstlink() {
  document
    .querySelector(".main-shots-titleWrapper .section-title:last-child")
    .classList.remove("hide");
}
function hideInstlink() {
  document
    .querySelector(".main-shots-titleWrapper .section-title:last-child")
    .classList.add("hide");
}
