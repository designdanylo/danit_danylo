// !Теоретический вопрос

// !Опишите своими словами разницу между функциями setTimeout() и setInterval().

// *setTimeout позволяет вызвать функцию один раз через определённый интервал времени. setInterval позволяет вызывать функцию регулярно, повторяя вызов через определённый интервал времени


// !Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?

// *Да она сработает мгновенно - потому что задерержки нету - интервал будет работаь один за другим 

// !Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

// *Потому что clearInterval() отменяет многократные повторения действий, установленные вызовом функции setInterval(). 




let endNumberImg = document.images.length;
let currentNumberImg = 1;
let currentImg = document.querySelector(`[src^="./img/${currentNumberImg}"]`);
const movementTime = 3000;
let timer = null;

function changeImage(number) {
  let newCurrentImg = document.querySelector(`[src^="./img/${number}."]`);

  if (currentImg) currentImg.classList.add("hide-img")
    currentImg = newCurrentImg;
    newCurrentImg.classList.remove("hide-img");
}

function nextSlide() {
  changeImage(currentNumberImg);
  currentNumberImg < endNumberImg ? currentNumberImg++ :  currentNumberImg = 1;
  timer = setTimeout(nextSlide, movementTime);
 } 

 nextSlide();

const stopBtn = document.getElementById('stopBtn');
const resumeBtn = document.getElementById('resumeBtn');
const btnConteiner = document.getElementById('btnConteiner');

btnConteiner.addEventListener('click', (event) => {
  const target = event.target;
  if (target.id === 'resumeBtn'){
    resumeBtn.classList.add('disabled');
    stopBtn.classList.remove('disabled')
    nextSlide();
  } else if(target.id === 'stopBtn'){
    resumeBtn.classList.remove('disabled');
    stopBtn.classList.add('disabled')
    clearInterval(timer)
  }
})




