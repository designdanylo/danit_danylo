// !Теоретический вопрос

// *Почему для работы с input не рекомендуется использовать события клавиатуры?

// *Потому что можно ввести значение разными способами помимо клавиатуры  - например голосом или мышкой

document.addEventListener('keydown', function (event) {
  document.querySelectorAll('.btn').forEach(element => {
    if (element.textContent === event.key)
      element.focus()
  });
})

