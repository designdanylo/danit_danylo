"use strict"


// !Теоретический вопрос
// Опишите своими словами, как Вы понимаете, что такое обработчик событий.

// *Это  функция, которая обрабатывает, или откликается на собитие которое произошло

const getElement = document.getElementsByName('getPrice')[0];

const getRemoveBtn = () => {
    return `<span class="remove-button" onclick="removeItem(this)">x</span>`;
};

const showError = () => {
    getElement.classList.add("price-error");
    document.querySelector('.error').style.display = 'block';
};

const hideError = () => {
    getElement.classList.remove("price-error");
    document.querySelector('.error').style.display = ''
};

function createNewElement(price) {
    return `<span class="current-price">Ваша цена: ${price} ${getRemoveBtn()} </span>`
}

function removeItem(element) {
    element.closest('.current-price').remove();
}

function isValidValue(value) {
    return !value ? false : true
}

function addNewElement() {
    const inputValue = document.getElementsByName('getPrice')[0].value;

    if (isValidValue(inputValue) && inputValue >= 0) {
        hideError();
        getElement.insertAdjacentHTML("beforebegin", createNewElement(inputValue));
        document.querySelector("input").value = "";
    } else if (inputValue < 0) {
        showError();
    }
}


getElement.onblur = addNewElement;

