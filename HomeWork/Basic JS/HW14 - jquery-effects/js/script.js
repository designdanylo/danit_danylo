$(document).ready(function () {
   $('#anchor').on('click', 'a', function (event) {
      event.preventDefault();
      const id = $(this).attr('href'), 
         top = $(id).offset().top;
      $('body,html').animate({ scrollTop: top }, 1000);
   });
});

$(document).ready(function () {
   $(window).scroll(function () {
      if ($(this).scrollTop() > $(window).height()) {
         $('#scrollUp').fadeIn();
      } else {
         $('#scrollUp').fadeOut();
      }
   });
   $('#scrollUp').click(function () {
      $('html, body').animate({ scrollTop: 0 }, 1000);
      return false;
   });
});

$('#roll').click(function () {
   $('.pop-post').slideToggle('slow');
});

