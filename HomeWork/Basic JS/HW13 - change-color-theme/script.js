const page = document.getElementById("document");
page.setAttribute("data-theme", localStorage.getItem("Theme"));

document.addEventListener("DOMContentLoaded", function (event) {
  let themeSwitcher = document.getElementById("theme-switcher");

  themeSwitcher.addEventListener("click", () => {
    let currentTheme = page.getAttribute("data-theme");
    currentTheme === "dark" ? page.setAttribute("data-theme", "light"): page.setAttribute("data-theme", "dark");
    localStorage.setItem("Theme", currentTheme === "dark" ? "light" : "dark");
  });
});
