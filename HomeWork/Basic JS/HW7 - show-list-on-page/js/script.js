"use strict"


// !Теоретический вопрос
// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)

// DOM-дерево представляет собой это объектную модель документа, которую браузер создаёт в памяти компьютера на основании HTML-кода, полученного им от сервера

function giveList(array, parent = document.body) {
  const list = document.createElement('ul')
   parent.append(list)
  let newArray = array.map(element => `<li>${element}</li>`);
  list.insertAdjacentHTML("afterBegin", newArray);
}

giveList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], document.querySelector('#root'))


