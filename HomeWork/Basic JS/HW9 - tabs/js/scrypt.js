
let selectedTabs = document.querySelector('.tabs .active');;

const nameListConteiner = document.querySelector('.tabs');

nameListConteiner.addEventListener('click', (event)=>{
  switchTabs(event.target);
});


function switchTabs(tab) {
  if (selectedTabs) {
    selectedTabs.classList.remove('active');
  }
  
    selectedTabs = tab;
    selectedTabs.classList.add('active')
  document.querySelectorAll(`.tabs-content li`).forEach(element => {
    if(element.dataset.switchtext === selectedTabs.dataset.switch){
      element.classList.add('active');
    } else {
      element.classList.remove('active');
    }
  })
}
