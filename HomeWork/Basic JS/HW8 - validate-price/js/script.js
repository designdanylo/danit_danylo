"use strict"


// !Теоретический вопрос
// Опишите своими словами, как Вы понимаете, что такое обработчик событий.

// *Это  функция, которая обрабатывает, или откликается на собитие которое произошло


const elementDiv = document.getElementById('root');

const textSpan = document.createElement('span');
textSpan.innerText = 'Price';
textSpan.className = 'styles-paragraph';

const priceInput = document.createElement('input');
priceInput.type = 'number'
priceInput.className = 'style-input'
priceInput.addEventListener('blur', (event) => {
const elementSpan = document.createElement ('span')
  

 if(priceInput.value > 0) {
  const elementSecondDiv = document.createElement('div');
  elementSecondDiv.className = 'container'
  elementSpan.innerHTML = `Текущая цена: ${priceInput.value}`
  const elementCrosswise = document.createElement('button')
  elementCrosswise.className = 'closed-btn'
  elementCrosswise.innerText = 'x';
  elementSecondDiv.append(elementSpan, elementCrosswise);
  elementDiv.prepend(elementSecondDiv)
  priceInput.classList.add('style-green');

  
  elementCrosswise.addEventListener("click", () => {
    priceInput.value = '';
    elementSecondDiv.remove();
    elementCrosswise.remove();
 })

 }else{
    priceInput.classList.add('style-red');
    priceInput.value = '';

    const elementSpan = document.createElement('span');
    elementSpan.className = 'error-text'
    elementSpan.innerText = "Please enter correct price";
    document.body.after(elementSpan);
 }

})

elementDiv.append(textSpan, priceInput);





  